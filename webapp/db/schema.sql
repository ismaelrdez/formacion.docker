use db;

create table item (id INT AUTO_INCREMENT PRIMARY KEY, name VARCHAR(30), priority INT, status INT);

insert into item (name, priority, status) values ("Chorizos", 1, 0);

insert into item (name, priority, status) values ("Panceta", 2, 0);

insert into item (name, priority, status) values ("Hamburguesas", 3, 1);