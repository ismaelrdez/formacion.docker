import Vue from 'vue'
import Router from 'vue-router'
import 'bulma/css/bulma.css'

import GetExample from '@/components/GetExample'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'GetExample',
      component: GetExample
    }
  ]
})
