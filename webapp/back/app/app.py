from flask import Flask, request, jsonify
from flask.ext.mysql import MySQL
from flask_cors import CORS
import json
import config

app = Flask(__name__)
CORS(app)
mysql = MySQL()

# MySQL configurations
app.config['MYSQL_DATABASE_USER'] = config.MYSQL_DATABASE_USER
app.config['MYSQL_DATABASE_PASSWORD'] = config.MYSQL_DATABASE_PASSWORD
app.config['MYSQL_DATABASE_DB'] = config.MYSQL_DATABASE_DB
app.config['MYSQL_DATABASE_HOST'] = config.MYSQL_DATABASE_HOST
mysql.init_app(app)

@app.route('/items')
def api_items():
    cursor = mysql.connect().cursor()
    cursor.execute("SELECT * from item")
    result = [{cursor.description[index][0]:column for index, column in enumerate(value)} for value in cursor.fetchall()]
    return json.dumps(result)

@app.route('/items', methods = ['POST'])
def api_new_item():
    conn = mysql.connect()
    cursor = conn.cursor()
    cursor.execute('INSERT INTO item (name, priority, status) VALUES ("%s", "%d", "%d")' % (request.json['name'], request.json['priority'], (request.json['status'] if request.json['status'] else 0)))
    conn.commit();
    cursor.execute("SELECT id FROM item WHERE name = %s" % (request.json['name']))
    data = {cursor.description[index][0]:column for index, column in enumerate(cursor.fetchone())}
    return json.dumps(data)

@app.route('/items/<itemid>', methods = ['GET', 'PUT', 'DELETE'])
def api_item(itemid):
    conn = mysql.connect()
    cursor = conn.cursor()

    if request.method == 'GET':
        cursor.execute("SELECT * FROM item WHERE id = %s" % (itemid))
        data = {cursor.description[index][0]:column for index, column in enumerate(cursor.fetchone())}

    if request.method == 'DELETE':
        cursor.execute("DELETE FROM item WHERE id = %s" % (itemid))
        conn.commit();
        data = "OK"

    if request.method == 'PUT':
        cursor.execute('UPDATE item SET status=1 WHERE id = %s' % (itemid))
        conn.commit();
        data = "OK"

    return json.dumps(data)

if __name__ == '__main__':
    app.run(debug=True,host='0.0.0.0')
